/*
 *  Copyright (c) 2018 digitalSTROM.org, Zurich, Switzerland
 *
 *  This file is part of vdc-doorbird
 */

#include "gmock/gmock.h"
#include "ipersistentstoragewithrowid.hpp"

namespace p44 {


class PersistentStorageWithRowIdMock : public IPersistentStorageWithRowId
{
public:
  MOCK_METHOD0(load, void());
  MOCK_METHOD0(save, void());
};

} //namespace p44

