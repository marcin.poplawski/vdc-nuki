/*
 * nukioperation.hpp
 *
 *  Created on: Aug 23, 2018
 *      Author: vlad
 */

#pragma once

#include "p44vdc_common.hpp"
#include "weboperation.hpp"
#include "jsonoperation.hpp"

namespace p44 {

namespace nuki {

  class Operation : public JsonOperation
  {
    using inherited = JsonOperation;

  public:

    Operation(JsonClient& aJsonClient, const string& aUrlPath, JsonWebClientCB aCallback);
    void sendRequest() P44_OVERRIDE;
  };

} // namespace nuki
} // namespace p44
