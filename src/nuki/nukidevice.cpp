/*
 * nukidevice.cpp
 *
 *  Created on: Aug 10, 2018
 *      Author: vlad
 *
 *  This file is part of vdc-nuki
 */
// File scope debugging options
// - Set ALWAYS_DEBUG to 1 to enable DBGLOG output even in non-DEBUG builds of this file
#define ALWAYS_DEBUG 0
// - set FOCUSLOGLEVEL to non-zero log level (usually, 5,6, or 7==LOG_DEBUG) to get focus (extensive logging) for this file
//   Note: must be before including "logger.hpp" (or anything that includes "logger.hpp")
#define FOCUSLOGLEVEL 7

#include "makeintrusive.hpp"
#include "nukiaction.hpp"
#include "nukidevice.hpp"
#include "nukioperation.hpp"
#include "boost/signals2.hpp"
#include "typeddevicestate.hpp"

using namespace p44::nuki;
const string port = "8080";

p44::DsScenePtr DeviceSettings::newDefaultScene(SceneNo aSceneNo)
{
  ScenePtr scene = ScenePtr(new Scene(*this, aSceneNo));
  scene->setDefaultSceneValues(aSceneNo);
  return scene;
}

void Scene::setAction(const string& aActionName)
{
  value = 0;
  setDontCare(false);
  command = aActionName;
}

void Scene::setDefaultSceneValues(SceneNo aSceneNo)
{
  inherited::setDefaultSceneValues(aSceneNo);

  switch (aSceneNo) {
    case FIRE:
      setAction("std.ActDoorUnlock");
      break;
    default:
      setDontCare(true);
      break;
  }
  markClean();
}


Device::Device(p44::Vdc *aVdcP, const string& aNukiName, const string& aNukiId, boost::intrusive_ptr<IBridge> aBridgePtr) :
  inherited(aVdcP), bridgePtr(aBridgePtr)
{
  setName(aNukiName);
  setNukiId(aNukiId);
  setColorClass(class_white_singledevices);
  installSettings(make_intrusive<DeviceSettings>(*this));
  // - set a action output behaviour (no classic output properties and channels)
  OutputBehaviourPtr ab = make_intrusive<ActionOutputBehaviour>(*this);
  ab->setGroupMembership(group_black_variable, true);
  addBehaviour(ab);
}

Device::~Device()
{
  connection.disconnect();
}

bool Device::identifyDevice(IdentifyDeviceCB aIdentifyCB)
{
  deriveDsUid();
  configureDevice();
  getLockState();
  autoAddStandardActions();
  return true;
}

void Device::initializeDevice(StatusCB aCompletedCB, bool aFactoryReset)
{
  aCompletedCB(Error::ok());
}

void Device::configureDevice()
{
  deviceEvents->addEvent(make_intrusive<DeviceEvent>(DeviceEvent(*this, mapEventToString(Event::EvLocked).value(), "Event Locked")));
  deviceEvents->addEvent(make_intrusive<DeviceEvent>(DeviceEvent(*this, mapEventToString(Event::EvUnlocked).value(), "Unlocked")));
  deviceEvents->addEvent(make_intrusive<DeviceEvent>(DeviceEvent(*this, mapEventToString(Event::EvErrorState).value(), "Error State")));

  auto currentStateEnum = make_intrusive<TypedEnumValueDescriptor<StaLockState>>("StaLockState",
							make_pair("Uncalibrated", StaLockState::Uncalibrated),
							make_pair("Locked", StaLockState::Locked),
							make_pair("Unlocked", StaLockState::Unlocked),
							make_pair("Unlatched", StaLockState::Unlatched),
							make_pair("Lock and Go", StaLockState::LockAndGo),
							make_pair("Motor Blocked", StaLockState::MotorBlocked),
							make_pair("Undefined", StaLockState::Undefined)
						 );

  staLockState = make_intrusive<TypedDeviceState<StaLockState>>
		  	  	  (*this, "StaLockState", "StaLockState", currentStateEnum,
		  	  	    [](DeviceStatePtr aChangedState, DeviceEventsList &aEventsToPush){
	  	  	  	  	  LOG(LOG_DEBUG, "State changed");
				        }
		  	  	  );

  deviceStates->addState(staLockState);

  deviceActions->addAction(make_intrusive<Action>(*this, "ActUnlock", "Open Door", LockAction::Unlock));
  deviceActions->addAction(make_intrusive<Action>(*this, "ActLock", "Close Door", LockAction::Lock));
  deviceActions->addAction(make_intrusive<Action>(*this, "ActUnlatch", "Unlatch Door", LockAction::Unlatch));
  deviceActions->addAction(make_intrusive<Action>(*this, "ActLockAndGo", "Nuki Lock And Go", LockAction::LockAndGo));
  deviceActions->addAction(make_intrusive<Action>(*this, "ActLockAndGoWithUnlatch", "Nuki Lock And Go With Unlatch", LockAction::LockAndGoWithUnlatch));
}


string Device::hardwareGUID()
{
  return "nukideviceid:" + nukiId;
}


string Device::hardwareModelGUID()
{
  return "nukimodel: modelDesc";
}

string Device::hardwareVersion()
{
  return "fwVersion";
}

string Device::modelName()
{
  return "modelName";
}


string Device::vendorName()
{
  return "Nuki";
}

string Device::oemModelGUID()
{
  // temporary dummy number
  return "gs1:(01)1234567890123";
}

void Device::deriveDsUid()
{
  // vDC implementation specific UUID:
  DsUid vdcNamespace(DSUID_P44VDC_NAMESPACE_UUID);
  dSUID.setNameInSpace(hardwareGUID(), vdcNamespace);
}


string Device::description()
{
  return {};
}

p44::ErrorPtr Device::handleMethod(VdcApiRequestPtr aRequest, const string &aMethod, ApiValuePtr aParams)
{
  return SingleDevice::handleMethod(aRequest, aMethod, aParams);
}


void Device::lockAction(LockAction action, StatusCB aCompletedCB){
  LOG(LOG_DEBUG, "LockAction");
  if(auto act = getActionId(action)){
    if (auto url = buildRequest(DeviceRequest::LockAction, act)) {
      auto operation = make_intrusive<Operation>(bridgePtr->getJsonClient(), url.value(),
        [=](JsonObjectPtr aJsonResponse, ErrorPtr aError){
            if (!aJsonResponse) {
              if (aCompletedCB)  aCompletedCB(TextError::err("Error: No Json Response!"));
              return;
            }

            ErrorPtr returnError;

            if (auto success = aJsonResponse->get("success")){
              if (success->boolValue()) {
                returnError = Error::ok();
              }
              else {
                returnError = TextError::err("Error: Action execution on device did not succeeded!");
              }
              LOG(LOG_DEBUG, "success = %s", success->stringValue().c_str());
            }
            else {
              returnError = TextError::err("Error: Json response does not contain success key!");
            }

            if (auto batteryCritical = aJsonResponse->get("batteryCritical")){
              LOG(LOG_DEBUG, "batteryCritical = %s", batteryCritical->stringValue().c_str());
            }
            if (aCompletedCB) aCompletedCB(returnError);
          }
      );

    bridgePtr->getJsonClient().enqueueAndProcessOperation(operation);
    }
    else {
      if (aCompletedCB) aCompletedCB(TextError::err("Error: Incorrect Request!"));
      LOG(LOG_ERR, "Incorrect Request!");
    }
  }
  else {
    if (aCompletedCB) aCompletedCB(TextError::err("Error: Incorrect LockAction!"));
    LOG(LOG_ERR, "Incorrect LockAction! Can not submit query!");
  }
}


boost::optional<string> Device::getActionId(LockAction lockAction){
  switch(lockAction){
    case LockAction::Unlock:
      return string("1");
    case LockAction::Lock:
      return string("2");
    case LockAction::Unlatch:
      return string("3");
    case LockAction::LockAndGo:
      return string("4");
    case LockAction::LockAndGoWithUnlatch:
      return string("5");
    default :
      LOG(LOG_ERR, "Incorrect LockAction!");
      return boost::none;
  }
}

void Device::getLockState(){
  if (auto url = buildRequest(DeviceRequest::LockState)){
    auto operation = make_intrusive<Operation>(bridgePtr->getJsonClient(), url.value(),
      [=](JsonObjectPtr aJsonResponse, ErrorPtr aError){
          if (!aJsonResponse) return;

          if (auto state = aJsonResponse->get("state")){
            LOG(LOG_DEBUG, "state = %s", state->stringValue().c_str());
          }
          if (auto stateName = aJsonResponse->get("stateName")){
            LOG(LOG_DEBUG, "stateName = %s", stateName->stringValue().c_str());
            if (auto enumState = mapStringToStaLockState(stateName->stringValue())){
              staLockState->setAndPushValue(enumState.value());
            }
          }
          if (auto success = aJsonResponse->get("success")){
            LOG(LOG_DEBUG, "success = %s", success->stringValue().c_str());
          }
        }
    );
    bridgePtr->getJsonClient().enqueueAndProcessOperation(operation);
  }
}

boost::optional<string> Device::buildRequest(DeviceRequest aRequest, boost::optional<string> action)
{
  const string url = "http://" + bridgePtr->getIp() + ":" + port;
  if (!(bridgePtr->getToken().empty())){
    switch(aRequest){
    case DeviceRequest::LockAction:
      return url + "/lockAction?nukiId=" + nukiId + "&action=" + action.value() +"&token=" + bridgePtr->getToken();
    case DeviceRequest::LockState:
      return url + "/lockState?nukiId=" + nukiId + "&token=" + bridgePtr->getToken();
    case DeviceRequest::Unpair:
      return url + "/unpair?nukiId=" + nukiId + "&token=" + bridgePtr->getToken();
    default:
      LOG(LOG_ERR, "Incorrect command!");
      return boost::none;
    }
  } else return boost::none;
}


void Device::callbackDataHandler(JsonObjectPtr& aJsonResponse, p44::ErrorPtr aError){
  if (!aJsonResponse) return;

  auto nukiId = aJsonResponse->get("nukiId");

  if(!nukiId) return;
  if(this->nukiId != nukiId->stringValue()) return;

  LOG(LOG_DEBUG, "Lock JSON response FROM DEVICE SLOT, nukiId = %s",
      nukiId->stringValue().c_str());

  if(auto jsonReponse = aJsonResponse->get("stateName")){
    string stateName = jsonReponse->stringValue();

    LOG(LOG_DEBUG, "stateName = %s", stateName.c_str());

    if(auto st = mapStringToStaLockState(stateName)){
      if(!staLockState->set(st.value())) return;
      if(auto ev = mapEvnetToState(st.value()))
        staLockState->pushWithEvent(ev);
    }
  }
}

boost::optional<string> Device::mapEventToString(Event aEvent){
  switch(aEvent){
    case Event::EvLocked:
      return string("EvLocked");
    case Event::EvUnlocked:
      return string("EvUnlocked");
    case Event::EvErrorState:
      return string("EvErrorState");
    default :
      LOG(LOG_ERR, "Incorrect Event!");
      return boost::none;
  }
}

boost::optional<StaLockState> Device::mapStringToStaLockState(const string & aStaLockState){
  if(aStaLockState == "locked")
    return StaLockState::Locked;
  else if(aStaLockState == "unlocked")
    return StaLockState::Unlocked;
  else if(aStaLockState == "undefined")
    return StaLockState::Undefined;
  else if(aStaLockState == "motor blocked")
    return StaLockState::MotorBlocked;
  else if(aStaLockState == "uncalibrated")
    return StaLockState::Uncalibrated;
  else if(aStaLockState == "unlatched")
    return StaLockState::Unlatched;
  else if(aStaLockState == "unlocked (lock 'n' go)")
    return StaLockState::LockAndGo;
  else {
    LOG(LOG_ERR, "Received unexpected state: %s", aStaLockState.c_str());
    return boost::none;
  }
}

p44::DeviceEventPtr Device::mapEvnetToState(const StaLockState & aStaLockState){
  Event event;

  switch(aStaLockState){
    case StaLockState::Locked: event = Event::EvLocked; break;
    case StaLockState::Unlocked: event = Event::EvUnlocked; break;
    case StaLockState::Undefined: event = Event::EvErrorState; break;
    case StaLockState::MotorBlocked: event = Event::EvErrorState; break;
    case StaLockState::Uncalibrated: event = Event::EvErrorState; break;
    case StaLockState::Unlatched: event = Event::EvUnlocked; break;
    case StaLockState::LockAndGo: event = Event::EvUnlocked; break;
    default:
      return nullptr;
  }

  if(auto ev = mapEventToString(event))
    return deviceEvents->getEvent(ev.value());
  else
    return nullptr;
}


