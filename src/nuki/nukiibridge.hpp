/*
 * nukiibridge.hpp
 *
 *  Created on: Sep 6, 2018
 *      Author: vlad
 */
#pragma once
#include "jsonoperation.hpp"
#include "jsonwebclient.hpp"
#include "boost/signals2.hpp"
#include "boost/optional.hpp"
#include "callback.hpp"

namespace p44 {

namespace nuki {

  struct NameAndNukiId {
  	string name;
  	string nukiId;
  };

  enum class BridgeRequest{
    Auth,
    List,
    Info,
    AddCallback,
    ListCallback,
    RemoveCallback
  };

  static constexpr char* CHOSEN_PORT = "8777";
  static constexpr char* CALLBACK_ID = "0";

  using UpdateDataCB = boost::function<void(JsonObjectPtr)>;
  using GetLocksCB = Callback<void(std::list<NameAndNukiId> aNameAndNukiesIdList)>;
  using GetCallbacksCB = Callback<void(std::vector<std::pair<std::string,std::string>> callbackList)>;

  class IBridge : public P44Obj{

    public:
	    virtual ~IBridge() {};

      virtual void authorize(StatusCB aStatusCB) = 0;

      virtual string getIp() const = 0;

      virtual void setToken(const string& aToken) = 0;

      virtual string getToken() const = 0;

      virtual void getLocks(GetLocksCB aNukiLocksCB) = 0;

      virtual JsonClient& getJsonClient() = 0;

      virtual boost::optional<string> buildRequest(BridgeRequest aRequest, string aCallbackId = CALLBACK_ID) const = 0;

      virtual boost::signals2::connection registerCallback(UpdateDataCB aCallback) = 0;

      virtual bool getCallbackState() = 0;
  };

} // namespace nuki
} // namespace p44
