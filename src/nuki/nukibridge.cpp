/*
 * nukibridge.cpp
 *
 *  Created on: Aug 29, 2018
 *      Author: vlad
 */
// File scope debugging options
// - Set ALWAYS_DEBUG to 1 to enable DBGLOG output even in non-DEBUG builds of this file
#define ALWAYS_DEBUG 0
// - set FOCUSLOGLEVEL to non-zero log level (usually, 5,6, or 7==LOG_DEBUG) to get focus (extensive logging) for this file
//   Note: must be before including "logger.hpp" (or anything that includes "logger.hpp")
#define FOCUSLOGLEVEL 6


#include "nukivdc.hpp"
#include "utils.hpp"
#include "macaddress.hpp"
#include "mainloop.hpp"
#include "makeintrusive.hpp"
#include "macaddress.hpp"
#include "boost/algorithm/string/predicate.hpp"
#include "iconnectable.hpp"
#include "persistentstoragefactory.hpp"
#include "nukioperation.hpp"
#include "nukibridge.hpp"
#include <string>
#include <algorithm>

using namespace p44::nuki;

const static string port = "8080";


Bridge::Bridge(JsonClient & aJsonClient, const string& aIp, const string& aMacAddress,
    const string& aSslCAFile, IPersistentStorageFactory& aStorageFactory, const string& aDsUID) :
    jsonClient(aJsonClient),
    ip(aIp),
    dsUID(aDsUID)
{
  jsonClient.isMemberVariable();
  jsonClient.getApi().setServerCertVfyDir(aSslCAFile);

  aStorageFactory.addRecord({ "accessToken", accessToken });

  storage = aStorageFactory.createStorageWithRowId(aMacAddress, "BridgeSettings");

  storage->load();
  LOG(LOG_DEBUG, "READ accessToken = %s", accessToken.c_str());
}

void Bridge::setToken(const string& aToken)
{
  accessToken = aToken;
  storage->save();
}

string Bridge::getToken() const
{
  return accessToken;
}

string Bridge::getIp() const
{
  return ip;
}

void Bridge::authorize(StatusCB aStatusCB)
{
  if(!accessToken.empty()) {
    LOG(LOG_DEBUG, "This Bridge already was authorized. Its accessToken = %s",
        accessToken.c_str());
    configureCallback([&, aStatusCB](ErrorPtr aError){
      setCallbackFlag(aError, aStatusCB);
    });
    return;
  }

  if (auto url = buildRequest(BridgeRequest::Auth)){
    auto operation = make_intrusive<Operation>(jsonClient, url.value(),
      [&, aStatusCB](JsonObjectPtr aJsonResponse, ErrorPtr aError){
        LOG(LOG_NOTICE, "Push down Bridge's button to authorization");
        if (!aJsonResponse) return;

        if (auto token = aJsonResponse->get("token")) {
          setToken(token->stringValue());
          LOG(LOG_NOTICE, "Token = %s", getToken().c_str());
          configureCallback([&, aStatusCB](ErrorPtr aError){
            setCallbackFlag(aError, aStatusCB);
          });
        }
        else {
          aStatusCB(TextError::err("No token !!!"));
        }
      });
    jsonClient.enqueueAndProcessOperation(operation);
  }
}

void Bridge::getLocks(GetLocksCB aNukiLocksCB)
{
  LOG(LOG_DEBUG, "Locks available for bridge with IP = %s", ip.c_str());
  if (auto url = buildRequest(BridgeRequest::List)){
    auto operation = make_intrusive<Operation>(jsonClient, url.value(),
     [=](JsonObjectPtr aJsonResponse, ErrorPtr aError){
          list<NameAndNukiId> nukisList;

          if(!aJsonResponse) return;

          for (int i =0; i < aJsonResponse->arrayLength(); i++) {
            auto element = aJsonResponse->arrayGet(i);
            if (!element) break;

            NameAndNukiId nameAndNukiId;
            if (auto nukiId = element->get("nukiId")){
              nameAndNukiId.nukiId = nukiId->stringValue();
              LOG(LOG_DEBUG, "nukiID = %s", nukiId->stringValue().c_str());
            }
            if (auto name = element->get("name")){
              nameAndNukiId.name = name->stringValue();
              LOG(LOG_DEBUG, "name = %s", name->stringValue().c_str());
            }
            nukisList.push_back(nameAndNukiId);
          }
          LOG(LOG_DEBUG, "Calling Lock CALLBACK");
          aNukiLocksCB(nukisList);
        }
    );
    jsonClient.enqueueAndProcessOperation(operation);
  }
}

void Bridge::listCallback(GetCallbacksCB aCallbackListCB){
  if (auto url = buildRequest(BridgeRequest::ListCallback)){
    auto operation = make_intrusive<Operation>(jsonClient, url.value(),
    [=](JsonObjectPtr aJsonResponse, ErrorPtr aError){

      if (!aJsonResponse) return;
      if (auto callbacks = aJsonResponse->get("callbacks")){
        std::vector<std::pair<std::string,std::string>> callbacksList;
        for (int i =0; i < callbacks->arrayLength(); ++i){
          if (auto element = callbacks->arrayGet(i)) {
            auto id = element->get("id");
            auto cUrl = element->get("url");
            if ((id) && (cUrl)) {
              callbacksList.emplace_back(std::make_pair(id->stringValue(),cUrl->stringValue()));
            }
          }
        }
        aCallbackListCB(callbacksList);
      }
		});
  jsonClient.enqueueAndProcessOperation(operation);
  }
}

void Bridge::removeCallback(const string& id)
{
  if (auto url = buildRequest(BridgeRequest::RemoveCallback, id)){
    auto operation = make_intrusive<Operation>(jsonClient, url.value(), [](auto...){});
    jsonClient.enqueueAndProcessOperation(operation);
  }
}

void Bridge::checkCallbackList(StatusCB aStatusCB){
  listCallback([=](std::vector<std::pair<std::string,std::string>> callbackList){
    if (callbackList.empty()) {
      aStatusCB(Error::ok());
      return;
    }
    auto definedCallback = "http://" + ipv4ToString(ipv4Address()) + ":" + CHOSEN_PORT + "?dsUID=" + dsUID;
    auto it = std::find_if(callbackList.begin(), callbackList.end(), [=](auto callback){
      return (callback.second == definedCallback);
    });
    if (it->second != definedCallback) {
      aStatusCB(Error::ok());
    } else {
      std::for_each(it + 1, callbackList.end(), [=](auto callback){
        if (callback.second == definedCallback) removeCallback(callback.first);
      });
      aStatusCB(TextError::err("Callback is already added!"));
    }
  });
}

void Bridge::configureCallback(StatusCB aStatusCB){
	checkCallbackList([=](ErrorPtr aError){
    if (aError->getErrorCode() == 0) {
      registerLockCallback([=](ErrorPtr aError){
        if (aError->getErrorCode() == 0) {
          aStatusCB(Error::ok());
          return;
        }
        aStatusCB(TextError::err("Error! Cannot register callback!"));
      });
      return;
    }
    aStatusCB(Error::ok());
  });
}

void Bridge::registerLockCallback(StatusCB aStatusCB)
{
  if (auto url = buildRequest(BridgeRequest::AddCallback)){
    auto operation = make_intrusive<Operation>(jsonClient, url.value(),
    [=](JsonObjectPtr aJsonResponse, ErrorPtr aError){
      if (!aJsonResponse) return;
      if (auto success = aJsonResponse->get("success")){
        if (success->boolValue()) aStatusCB(Error::ok());
        else {
          auto message = aJsonResponse->get("message");
          LOG(LOG_DEBUG, "Error : %s", message->stringValue().c_str());
          aStatusCB(TextError::err("Error: Cannot add callback!"));
        }
      }
    });
    jsonClient.enqueueAndProcessOperation(operation);
  }
}

void Bridge::setCallbackFlag(ErrorPtr aErrorPtr, StatusCB aStatusCB){
  if (aErrorPtr->getErrorCode() == 0){
    callbackOK = true;
    aStatusCB(Error::ok());
  }
  else {
    callbackOK = false;
    aStatusCB(TextError::err("Error: Cannot configure callback!"));
  }
}

boost::optional<string> Bridge::buildRequest(BridgeRequest aRequest, string aCallbackId) const
{
  const string url = "http://" + ip + ":" + port;
  if (aRequest == BridgeRequest::Auth) return url + "/auth";
  if (getToken().empty()) {
    return boost::none;
  }
  switch(aRequest){
  case BridgeRequest::List:
    return url + "/list?token=" + accessToken;
  case BridgeRequest::Info:
    return url + "/info?token=" + accessToken;
  case BridgeRequest::AddCallback:
    return url + "/callback/add?url=http%3A%2F%2F" + ipv4ToString(ipv4Address()) + "%3A" + CHOSEN_PORT + "%3FdsUID=" + dsUID + "&token=" + accessToken;
  case BridgeRequest::RemoveCallback:
    return url + "/callback/remove?id=" + aCallbackId + "&token=" + accessToken;
  case BridgeRequest::ListCallback:
    return url + "/callback/list?&token=" + accessToken;
  default:
    return boost::none;
  }
}

boost::signals2::connection Bridge::registerCallback(UpdateDataCB aCallback)
{
  return {};
}

void Bridge::cancelPollCycle()
{
  pollDataTicket.cancel();
}

void Bridge::pollCycle()
{
  pollDataTicket.executeOnce([&](auto...){ this->pollCycle(); }, POLLING_INTERVAL);
}
