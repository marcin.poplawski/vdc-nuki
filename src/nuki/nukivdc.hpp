/*
 * nukivdc.hpp
 *
 *  Created on: Aug 10, 2018
 *      Author: vlad
 */

#pragma once
#include "p44vdc_common.hpp"
#include "vdc.hpp"
#include "persistentstorage.hpp"
#include "nukidevice.hpp"
#include "jsonobject.hpp"
#include "jsonwebclient.hpp"
#include "nukibridge.hpp"
#include "httpserver.hpp"
#include "boost/signals2.hpp"
#include "jsonobject.hpp"

namespace p44 {

namespace nuki {

  using StringList = std::list<string>;

  class Vdc : public PersistentStorage<p44::Vdc>
  {
    using inherited = PersistentStorage<p44::Vdc>;
    using GetBridgesCB = Callback<void()>;

    JsonClient jsonClient;
    StringList hostIPs;

  public:
    Vdc(int aInstanceNumber, VdcHost *aVdcHostP, int aTag);

    virtual const char *vdcClassIdentifier() const P44_OVERRIDE;
    virtual int getRescanModes() const P44_OVERRIDE;
    virtual string vdcModelSuffix() const;
    bool getDeviceIcon(string &aIcon, bool aWithData, const char *aResolutionPrefix);
    virtual void scanForDevices(StatusCB aCompletedCB, RescanMode aRescanFlags);
    virtual bool dynamicDefinitions() P44_OVERRIDE;
    void getBridges(GetBridgesCB bridgesVectorCB);
    JsonClient& getJsonClient();
    string getDsUID() const { return this->dSUID.getString(); }

    boost::signals2::signal<void (JsonObjectPtr &, p44::ErrorPtr)> sig;

  private:
    void callGetLocksWithCB(boost::intrusive_ptr<p44::nuki::Bridge>& br);

    std::list<boost::intrusive_ptr<Bridge>> bridgesList;
    std::unique_ptr<HttpServer> httpServer;
    const string port = "8777";
    const string ip = "localhost";
  };

} // namespace nuki
} // namespace p44
