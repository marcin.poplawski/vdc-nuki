/*
 * nukiaction.hpp
 *
 *  Created on: Aug 21, 2018
 *      Author: vlad
 *
 *
 *  This file is part of vdc-nuki
 */

#pragma once
#include "p44vdc_common.hpp"
#include "singledevice.hpp"
#include "nukidevice.hpp"

namespace p44 {

namespace nuki {

  class Action : public DeviceAction
  {

  LockAction action;
  Device &device;
  using inherited = DeviceAction;

  public:
    Action(Device &aDevice, const string& aId, const string& aDescription, LockAction aLockAction);

    virtual void performCall(ApiValuePtr aParams, StatusCB aCompletedCB) P44_OVERRIDE;
  };
  using ActionPtr = boost::intrusive_ptr<Action>;

} // namespace nuki
} // namespace p44
