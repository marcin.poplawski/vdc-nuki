/*
 * nukivdc.cpp
 *
 *  Created on: Aug 10, 2018
 *      Author: vlad
 */
// File scope debugging options
// - Set ALWAYS_DEBUG to 1 to enable DBGLOG output even in non-DEBUG builds of this file
#define ALWAYS_DEBUG 0
// - set FOCUSLOGLEVEL to non-zero log level (usually, 5,6, or 7==LOG_DEBUG) to get focus (extensive logging) for this file
//   Note: must be before including "logger.hpp" (or anything that includes "logger.hpp")
#define FOCUSLOGLEVEL 6

#include "nukivdc.hpp"

#include "mainloop.hpp"
#include "makeintrusive.hpp"
#include "macaddress.hpp"
#include "boost/algorithm/string/predicate.hpp"
#include "iconnectable.hpp"
#include "persistentstoragefactory.hpp"
#include "nukioperation.hpp"
#include "nukidevice.hpp"
#include "nukiibridge.hpp"
#include "boost/signals2/connection.hpp"

#include "httpserver.hpp"

using namespace p44::nuki;

namespace
{

string macAddressFromHostName(const string& aHostName)
{
  if (aHostName.empty()) {
    return {};
  }

  static const string PREFIX = "bha-";
  static const size_t MAC_LENGTH = 12;

  if (!boost::starts_with(aHostName, PREFIX)) {
    LOG(LOG_DEBUG, "Cannot convert host name %s to mac address", aHostName.c_str());
    return {};
  }

  return aHostName.substr(PREFIX.size(), MAC_LENGTH);
}

string macAddressFromIp(const string& aIp)
{
  uint64_t macAddress;
  if (!p44::getMacAddressByIpv4(p44::stringToIpv4(aIp.c_str()), macAddress)) {
    LOG(LOG_DEBUG, "Cannot get device mac address (IP : %s)", aIp.c_str());
    return {};
  }
  return p44::macAddressToString(macAddress);
}

}


Vdc::Vdc(int aInstanceNumber, VdcHost *aVdcHostP, int aTag) :
  inherited(std::make_tuple(PersistentRecord<StringList>("hostIPs", hostIPs)),
            aInstanceNumber,
            aVdcHostP,
            aTag)
{
  jsonClient.isMemberVariable();
  initializeName("Nuki Controller");

  httpServer = make_unique<HttpServer>(MainLoop::currentMainLoop(), ip, port);
  httpServer->startServer();
  httpServer->setJsonDataHandler([=](JsonObjectPtr aJsonResponse, ErrorPtr aError){
    if(!aJsonResponse) return;
    if(auto jsonResponse = aJsonResponse->get("nukiId")){
      LOG(LOG_DEBUG, "Lock JSON response FROM CALLBACK = %s", jsonResponse->stringValue().c_str());
    }else {
      LOG(LOG_ERR, "Server received unexpected JSON");
    }
    sig(aJsonResponse, aError);
  });
  httpServer->isMemberVariable();
}

const char *Vdc::vdcClassIdentifier() const
{
  return "Nuki_Container";
}


bool Vdc::getDeviceIcon(string &aIcon, bool aWithData, const char *aResolutionPrefix)
{
  if (getIcon("vdc_nuki", aIcon, aWithData, aResolutionPrefix))
    return true;
  else
    return inherited::getDeviceIcon(aIcon, aWithData, aResolutionPrefix);
}


int Vdc::getRescanModes() const
{
  // normal and incremental make sense, no exhaustive mode
  return rescanmode_incremental+rescanmode_normal;
}

string Vdc::vdcModelSuffix() const
{
  return "";
}

void Vdc::scanForDevices(StatusCB aCompletedCB, RescanMode aRescanFlags)
{
  getBridges([=](){
    for (auto& br:bridgesList) {
      br->authorize([&](ErrorPtr aError)
          { callGetLocksWithCB(br); });
    }
  });
  aCompletedCB(Error::ok());
}

bool Vdc::dynamicDefinitions(){
  return true;
}


p44::JsonClient& Vdc::getJsonClient()
{
  return jsonClient;
}

constexpr char* BRIDGE_DISCOVERY_URL =  "https://api.nuki.io/discover/bridges";
void Vdc::getBridges(GetBridgesCB bridgesVectorCB)
{
  auto operation = make_intrusive<Operation>(jsonClient, BRIDGE_DISCOVERY_URL,
      [=](JsonObjectPtr aJsonResponse, ErrorPtr aError){

    if (!aJsonResponse) return;

    auto bridges = aJsonResponse->get("bridges");
    if (!bridges) return;

    for (int i =0; i < bridges->arrayLength(); ++i) {
      auto element = bridges->arrayGet(i);
      if (!element) continue;

      auto ip = element->get("ip");
      if (!ip) continue;

      auto macAddress = macAddressFromIp(ip->stringValue());
      if(macAddress.empty()) continue;

      PersistentStorageFactory storageFactory(getVdcHost().getDsParamStore());
      bridgesList.emplace_back(make_intrusive<Bridge>(
          jsonClient, ip->stringValue(), macAddress, string(""), storageFactory, getDsUID()));
    }
    bridgesVectorCB();
  });
  jsonClient.enqueueAndProcessOperation(operation);
}

void Vdc::callGetLocksWithCB(boost::intrusive_ptr<p44::nuki::Bridge>& br){
  br->getLocks(
    [=, &br](std::list<NameAndNukiId> aNameAndNukiesIdList){
      for(auto& it:aNameAndNukiesIdList){
        auto device = make_intrusive<Device>(this, it.name.c_str(), it.nukiId.c_str(), br);
        //register slot for handling POST requests, responses from Nuki Lock
        boost::signals2::connection connection
        = sig.connect([=](JsonObjectPtr & aResponse, p44::ErrorPtr aError){
          device->callbackDataHandler(aResponse, aError);
        });
        device->setConnctionForSlot(connection);
        simpleIdentifyAndAddDevice(device);
        LOG(LOG_DEBUG, "Name FROM CALLBACK = %s", it.name.c_str());
        LOG(LOG_DEBUG, "NukiId FROM CALLBACK = %s", it.nukiId.c_str());
      }
    });
}

