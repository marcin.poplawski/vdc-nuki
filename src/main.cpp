/*
 * main.cpp
 *
 *  Created on: Aug 8, 2018
 *      Author: vlad
 */
#include "applicationvdc.hpp"
#include "vdctags.hpp"
#include "addonvdchost.hpp"
#include "makeintrusive.hpp"
#include "nukivdc.hpp"


using namespace p44;

// this tag is defined there temporarily
static constexpr int NUKI_TAG=99;

class ApplicanionName : public ApplicationVdc
{
public:
  using ApplicationVdc::ApplicationVdc;

  Options vdcOptions() P44_OVERRIDE
  {
    return {};
  }

  Vdcs createVdcs() P44_OVERRIDE
  {
    return {make_intrusive<nuki::Vdc>(1, vdcHost.get(), NUKI_TAG)};
  }

};

int main(int argc, char **argv)
{
  // prevent debug output before application.main scans command line
  SETLOGLEVEL(LOG_EMERG);
  SETERRLEVEL(LOG_EMERG, false); // messages, if any, go to stderr
  // create app with current mainloop
  auto vdcHost = make_intrusive<AddonVdcHost>();
  static ApplicanionName application(vdcHost);
  application.parseArgs(argc, argv);
  // pass control
  return application.main();
}
